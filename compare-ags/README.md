# Compare_AGS4.py

Compares two AGS4 files and reports the added, removed, and modified records for each table, exported as .xlsx file with changes highlighted in yellow.

compare_key_dict_tables function could be used to compare any databases for differences (some pre-processing required)

If running the process from the main file rather than importing, create a file in the folder called "params_file.py" and define these variables editing the filepaths as required 

FP1 = r'old_file.ags'

FP2 = r'new_file.ags'

OUTPUT_FP = r'output.xlsx'

To run in Jupyter notebook:

1. Copy Compare_AGS4.py contents to a new cell and edit filepaths of FP1, FP2 & output.xlsx as required

2. Download/save Compare_AGS4.py to folder with AGS files, create new jupyter notebook with:

from ags4_compare import compare_ags4_databases

compare_ags4_databases('old.ags','new.ags','diffs.xlsx')

Uploaded as opensource by Arup ground engineering, credit to Adam Comelio for producing the python script.