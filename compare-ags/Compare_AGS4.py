from python_ags4 import AGS4
from python_ags4.check import pick_standard_dictionary
from collections import defaultdict
import openpyxl
from openpyxl.styles import PatternFill, Color

yellow = Color(rgb='00FFFF00')
yellow_fill = PatternFill(patternType='solid', fgColor=yellow)

def get_table_keys(tables=None, dict_version: str='4.0.4'):
    standard_dictionary_path = pick_standard_dictionary(tables, dict_version)

    data, _ = AGS4.AGS4_to_dict(standard_dictionary_path)

    keys_dict = defaultdict(list)

    d = data['DICT']
    for row in zip(d['DICT_GRP'], d['DICT_HDNG'], d['DICT_STAT']):
        if 'key' in row[2].lower():
            keys_dict[row[0]].append(row[1])

    return keys_dict

def get_key_dict_tables(AGS_fp: str, table_keys_dict: dict=None, version: str='4.0.4'):
    if table_keys_dict is None:
        table_keys_dict = get_table_keys(AGS4.AGS4_to_dataframe(AGS_fp), version)

    data, _ = AGS4.AGS4_to_dict(AGS_fp)

    tables = dict()
    i = 0
    for table_name in data:
        keys_list = table_keys_dict[table_name]
        table_dict = dict()
        for row in zip(*data[table_name].values()):
            row_dict = dict(zip(data[table_name], row))
            for k in row_dict:
                try:
                    tmp = float(row_dict[k])
                    row_dict[k] = tmp
                except:
                    pass
            table_dict[tuple(row_dict[k] for k in keys_list)] = row_dict
        tables[table_name] = table_dict
    return tables

def compare_key_dict_tables(db1: dict, db2: dict):
    diffs = defaultdict(list)
    similar_tables = set(db1) & set(db2)
    removed_tables = set(db1) - set(db2)
    added_tables   = set(db2) - set(db1)
    for table_name in similar_tables:
        table  = db1[table_name]
        table2 = db2[table_name]
        removed  = set(table)  - set(table2)
        added    = set(table2) - set(table)
        modified = set(k for k in set(table) & set(table2) if table[k] != table2[k])
        for k in removed:
            diffs[table_name].append((table[k], 'Removed'))
        for k in added:
            diffs[table_name].append((table2[k], 'Added'))
        for k in modified:
            change_list = list()
            for k2, v in table[k].items():
                if v != table2[k][k2]:
                    change_list.append((k2, v, table2[k][k2]))
            msg = ', '.join(f'{col}: {old} -> {new}' for col, old, new in change_list)
            diffs[table_name].append((table2[k], msg))
    for table_name in removed_tables:
        table = db1[table_name]
        for k in table:
            diffs[table_name].append((table[k], 'Removed'))
    for table_name in added_tables:
        table = db2[table_name]
        for k in table:
            diffs[table_name].append((table[k], 'Added'))
    return diffs

def compare_ags4_databases(fp1: str, fp2: str, output_fp: str='output.xlsx', version: str='4.0.4'):
    table_keys_dict = get_table_keys(AGS4.AGS4_to_dataframe(fp1), version)

    db1 = get_key_dict_tables(fp1, table_keys_dict)
    db2 = get_key_dict_tables(fp2, table_keys_dict)

    diffs = compare_key_dict_tables(db1, db2)

    wb = openpyxl.Workbook()
    for table_name, rows in diffs.items():
        ws = wb.create_sheet(title=table_name)
        columns = list(rows[0][0]) + ['changes',]
        column_number_pairs = dict(zip(columns, range(1, len(columns) + 1)))
        for col, num in column_number_pairs.items():
            ws.cell(row=1, column=num, value=col)
        for r, (row_dict, changes) in enumerate(rows, start=2):
            row_dict['changes'] = changes
            for col, num in column_number_pairs.items():
                try:
                    ws.cell(row=r, column=num, value=row_dict[col].encode('utf-8'))
                except AttributeError as e:
                    ws.cell(row=r, column=num, value=row_dict[col])
            if changes not in {'Added', 'Removed'}:
                cols = [s.split(': ')[0] for s in changes.split(', ')]
                for col in cols:
                    try:
                        c = ws.cell(row=r, column=column_number_pairs[col])
                        c.fill = yellow_fill
                    except KeyError as e:
                        pass

    wb.save(filename=output_fp)

    return diffs


if __name__ == '__main__':
    import params_file
    data = compare_ags4_databases(
        params_file.FP1,
        params_file.FP2,
        params_file.OUTPUT_FP
    )
