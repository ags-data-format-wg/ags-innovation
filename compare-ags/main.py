from ags_quality_checker import AgsChecker
from pprint import pprint

with open("AGS_v403_testERES.AGS") as f:
    data = f.read()


ags = AgsChecker.parse_string(data)

errors = ags.validate()
pprint(errors)
