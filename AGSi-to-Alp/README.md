# AGSi to Oasys Alp example

Python script to parse AGSi json data (ground model and parameters) to Oasys Alp json file, and run analysis.
See '2023-05-03 Alp AGSi import automation.pptx' for detail

Uploaded as opensource by Arup ground engineering, credit to Faith Jamieson for producing the python script.