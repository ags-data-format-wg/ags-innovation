from csv import excel
from lib2to3.pgen2.pgen import DFAState
import win32com.client
import json
import os
import pandas as pd
import numpy as np
from  itertools import combinations
from openpyxl import load_workbook 
import datetime
from types import SimpleNamespace
from pprint import pprint
from os import path

jsonpath = r'C:\\Example_AGSI_P001.json' ### AGSI FILE MODEL
alpjson= r"C:\\P001 tester.json" ### EXISTING ALP FILE WITH OTHER DATA ALREADY INPUTTED
caseID = 'Pile design'


def alp_analyse(alpjson):
    try:
        os.system('TASKKILL /F /IM alp.exe')
    except:
        print("no alp running before start - continue script")
    alp = win32com.client.Dispatch("alpLib_20_0.AlpAuto")
    file = os.path.abspath(alpjson)
    alp.Open(alpjson)
    alp.Save()
    alp.Analyse()
    alp.Save()
    alp.Close()
    print("finished analysis")


def ground_water(gframes,alpjson, casedID):
    ground_water=[]
    

    df_all_cols = pd.concat(gframes, axis = 0).reset_index(drop=True)
    #df_all_cols = df_all_cols[['elementID','codeID','caseID','valueNumeric','valueProfile','agsiGeometry.topElevation']]
    df_all_cols = df_all_cols.loc[df_all_cols['caseID'] == caseID]
    pivoted = df_all_cols.pivot('elementID','codeID','valueNumeric')
   

    lookup = df_all_cols.drop_duplicates('elementID')[['elementID', 'agsiGeometry.elevation']]
    lookup.set_index(['elementID'], inplace=True)
    df_all_cols =pivoted.join(lookup)
    print(df_all_cols)
    
    for i,row in df_all_cols.iterrows():
        with open(alpjson) as f:
            data = json.load(f)
        value = {"Level":{"Value":row['agsiGeometry.elevation'],"Dimension":"LENGTH"},"Pressure":{"Value":row['WaterPressure'],"Dimension":"PRESSURE"},"UnitWeight":{"Value":row['UnitWeightWater'],"Dimension":"WEIGHT_DENSITY"}}        
        ground_water.append(value)
        data['Water Data'] = ground_water
        with open(alpjson, 'w') as fp:
            json.dump(data, fp)
         ##refuse Nan Value   



def elem_check (jsonpath,alpjson, caseID):
    with open(jsonpath) as project_file:    
        data = json.load(project_file)  
    Model = data['agsiModel'][0]['agsiModelElement']
    soil_frames =[]
    gframes = []
    for elem in Model:
        #if 'agsiDataParameterValue' in elem.keys():
        #    recordkey = 'agsiDataParameterValue'
       # elif 'agsiDataPropertyValue' in elem.keys():
        #    recordkey = 'agsiDataPropertyValue'
        if elem['elementType'] == 'Groundwater':
            recordkey= 'agsiDataParameterValue'
            df1=pd.json_normalize(elem, record_path= recordkey, meta=[['elementID'],['agsiGeometry','elevation',]] , errors='ignore')
            gframes.append(df1)
        elif elem['elementType'] == 'Geotechnical unit':
            recordkey = 'agsiDataParameterValue'
            df2= pd.json_normalize(elem, record_path= recordkey, meta=[['elementID'],['agsiGeometry','topElevation',]], errors='ignore')
            soil_frames.append(df2)

    #print(gframes)
    print(soil_frames)
    ground_strat(soil_frames, alpjson,caseID)
    ground_water( gframes,alpjson,caseID)       
    alp_analyse(alpjson)



  
def ground_strat(soil_frames, alpjson,caseID):
    df_all_cols = pd.concat(soil_frames, axis = 0).reset_index(drop=True)
    

    df_all_cols = df_all_cols[['elementID','codeID','caseID','valueNumeric','valueProfile','agsiGeometry.topElevation']]
    df_all_cols = df_all_cols.loc[df_all_cols['caseID'] == caseID]
    pivoted = df_all_cols.pivot('elementID','codeID','valueNumeric')
    print(df_all_cols)

    lookup = df_all_cols.drop_duplicates('elementID')[['elementID', 'agsiGeometry.topElevation']]
    lookup.set_index(['elementID'], inplace=True)

    valueprofile = df_all_cols[['elementID','codeID','valueProfile']]
    valueprofile = valueprofile[valueprofile['codeID'].str.contains('Cohesion')]
    valueprofile = valueprofile[['elementID','valueProfile']]
    valueprofile.set_index(['elementID'],inplace=True)
    #print(valueprofile)
    valueprofile["valueProfile"] = valueprofile["valueProfile"].str[0]
    valueprofile['valueProfile'] = valueprofile['valueProfile'].fillna(0)
    #print(valueprofile)
    # df = pd.DataFrame({"A": [[1,2],[3,4],[8,9],[2,6]]})
    for i, row in valueprofile.iterrows():
        num = row['valueProfile']
        if  num == 0:
        #all (n==0 for n in num):
            row['valueProfile']=0
        else:
            y1 = num[0][0]
            x1 = num[0][1]
            y2 = num[1][0]
            x2 = num[1][1]
            grad = (x2-x1)/(y2-y1)
            row['valueProfile'] = grad

    df_all_cols =pivoted.join(lookup)
    df_all_cols = df_all_cols.join(valueprofile)
    
    df_all_cols=df_all_cols.fillna(0)
    print(df_all_cols)
    soil_data=[]
    for i,row in df_all_cols.iterrows():

        with open(alpjson) as f:
            data = json.load(f)
        values = {'id': 0,'SoilType': 'ELASTIC_PLASTIC', 'E_ref':{'Value': row['YoungsModulusDrained']*1000}, 'UnitWt':{'Value': row['UnitWeightBulk']*1000},"PhiBased":True,"Kq":0.0,"Kc":0.0,'C_ref':{'Value':row['Cohesion']*1000},"C_grad":row['valueProfile']*1000,'Phi':{'Value': row['AngleFrictionPeak']}, 'Toplevel':{'Value': row['agsiGeometry.topElevation']}} 
        soil_data.append(values)
        data['Soil Data'] = soil_data
        with open(alpjson, 'w') as fp:
            json.dump(data, fp)


elem_check(jsonpath, alpjson,caseID)
#### take input for if pile design or not then run for case IDS 
#### loop through EACH model not just first
#### mutiple ground water 
#### Pressure OR unit weight
#### if main
#### args parse
#### us alp COMM to actually run the analysis and creates a graph
#small multiples for i and m seasons

##lvdt
###
### not horizaontal may have piles inbetween, way to find out location of elevations is there differenced between 