#!/usr/bin/env python
# coding: utf-8

# DF 26/11/2024 Example python script for DQAL (data quality group), proof of concept only - very primative

#import pandas as pd
from python_ags4 import AGS4

AGSFilePath = 'data\AGS_v403_testERES.AGS'  # AGS file path
warnings_txt = 'output\DQ_checks.txt'
IgnoreAGSDict = (False)  # ABBR often includes DICT lookups by default, choose to ignore these

tables, headings = AGS4.AGS4_to_dataframe(AGSFilePath) #Import AGS file

#Extract data quality group
df_DQAL = tables["DQAL"]  # extract DQAL to dataframe, add error handling if not found
df_DQAL = df_DQAL.loc[df_DQAL["HEADING"] == "DATA"]  # filter to data only
df_DQAL.head(n=10)

text_file = open(warnings_txt, "w") #open text file for editing

for index, row in df_DQAL.iterrows():
    CheckGroup = row["DQAL_GRP"]
    CheckHeading = row["DQAL_HDNG"]
    CheckFilter = row["DQAL_FIL"]
    CheckUnits = row["DQAL_UNIT"]
    CheckSeverity = row["DQAL_SEV"] +","

    if row["DQAL_TYPE"] == "Completeness": #check heading is complete
        CheckFilterHeading = CheckFilter.split("=")[0].strip()
        CheckFilterValue = CheckFilter.split("=")[1].strip()
        #print(CheckFilterValue)
        #print(CheckGroup,CheckHeading,CheckFilter)
        df_GroupCheck = tables[CheckGroup]
        df_GroupCheck = df_GroupCheck.loc[df_GroupCheck["HEADING"] == "DATA"]
        df_GroupCheck = df_GroupCheck.loc[df_GroupCheck[CheckFilterHeading] == CheckFilterValue]
        for index1, row1 in df_GroupCheck.iterrows():
            #print(row1["SAMP_DTIM"])
            if row1[CheckHeading] =='':
                text_file.write(CheckSeverity + row1["LOCA_ID"]+","+row1["SAMP_TOP"]+","+row1["SAMP_REF"]+","+row1["SAMP_TYPE"]+","+"data not complete"+ "\n") #update to report group keyset

    if row["DQAL_TYPE"] == "Precision": #check precision is as defined
        if CheckUnits == "2DP":
            df_GroupCheck = tables[CheckGroup]
            df_GroupCheck = df_GroupCheck.loc[df_GroupCheck["HEADING"] == "DATA"]
            for index2, row2 in df_GroupCheck.iterrows():
                decimal = len(row2[CheckHeading].split('.')[1])
                if decimal != 2:
                    text_file.write(CheckSeverity + row2["LOCA_ID"]+ " not required units:"+ CheckUnits + "\n") #update to report group keyset

if "ABBR check" in df_DQAL['DQAL_TYPE'].values: #check ABBR list is in DQAL, if one or more ABBR checks exist
    df_DQAL_ABBR = df_DQAL.loc[df_DQAL["DQAL_TYPE"] == "ABBR check"]
    df_DQAL_ABBR['HDNG+VAL'] = df_DQAL_ABBR['DQAL_HDNG'].astype(str) + "+" + df_DQAL_ABBR['DQAL_VAL'].astype(str)

    df_abbr = tables["ABBR"]
    print(f"{df_abbr = }")
    print(df_abbr.info())
    df_abbr = df_abbr.loc[df_abbr["HEADING"] == "DATA"]
    df_abbr['HDNG+CODE'] = df_abbr['ABBR_HDNG'].astype(str) + "+" + df_abbr['ABBR_CODE'].astype(str)
    print(f"{df_abbr = }")
    print(df_abbr.info())
    for index, row in df_abbr.iterrows():
        if row["HDNG+CODE"] not in df_DQAL_ABBR['HDNG+VAL'].values:
            text_file.write ("warning,"+row["ABBR_CODE"]+","+row["ABBR_DESC"]+","+"not in DQAL abbr list"+ "\n") #update to report group keyset

text_file.close() #close text file for editing

