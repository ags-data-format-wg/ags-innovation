import json
import sys
import os
import pandas as pd

agsi_schema_filename = 'AGSiTemplate.json'
conversion_table_filename = 'GoferCodeList_Rev4.xlsx'

# Gets the agsi schema 
# Not strictly necessary but helps check if fields are populated
def get_template_model():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    agsi_schema_file = os.path.join(dir_path, agsi_schema_filename)
    
    with open(agsi_schema_file, 'r') as f:
        schema = json.load(f)
        return schema
    
# Adds data that is consistent across different models
def add_static_data(model):
    new_model = model.copy()
    
    new_model["agsFile"] = {
        "title": "Test from Gofer",
        "projectTitle": "Arup",
        "description": "Example AGSi dataset for scripting Oasys Gofer to AGSi",
        "producedBy": "Arup",
        "fileURI": "",
        "revision": "P01.1",
        "suitabilityType": "For Information",
        "status": "S3",
        "madeBy": "Arup",
        "checkedBy": "TBC",
        "approvedBy": "TBC"
    },
    
    new_model["agsSchema"] = {
        "name": "AGSi",
        "version": "1.0.0",
        "link": "https://ags-data-format-wg.gitlab.io/AGSi_Documentation/"
    }
    
    new_model["agsProject"]["agsProjectCoordinateSystem"] = [
        {
            "systemID": "TypicalSectionElevation",
            "description": "Typical section elevation",
            "systemType": "XY",
            "systemNameXY": "Typical section",
            "axisNameX": "Distance",
            "axisNameY": "Elevation",
            "axisUnitsXY": "m",
            "axisUnitsZ": " "
        }
    ]
    
    new_model["agsProject"]["agsProjectCodeSet"] = [
        {
            "description": "Parameter codes",
            "codeSetID": "CodeSetParameter",
            "usedByObject": "agsiDataParameterValue",
            "usedByAttribute": "codeID",
            "sourceDescription": "Gofer code list",
            "agsProjectCode": [
                {
                    "codeID": "UnitWeightBulk",
                    "description": "Bulk unit weight",
                    "units": "kN/m3",
                    "isStandard": True
                },
                {
                    "codeID": "YoungsModulusDrained",
                    "description": "Drained Young's Modulus",
                    "units": "MPa",
                    "isStandard": True
                },
                {
                    "codeID": "AngleFrictionPeak",
                    "description": "Peak effective angle of shearing resistance",
                    "units": "deg",
                    "isStandard": True
                },
                {
                    "codeID": "Cohesion",
                    "description": "Effective cohesion",
                    "units": "kPa",
                    "isStandard": True
                },
                {
                    "codeID": "UnitWeightWater",
                    "description": "Unit weight water",
                    "units": "kN/m3",
                    "isStandard": False
                },
                {
                    "codeID": "WaterPressure",
                    "description": "Water pressure",
                    "units": "kN/m2",
                    "isStandard": False
                },
                {
                    "codeID": "CoefficientLateralEarthPressureAtRest",
                    "description": "Coefficient lateral earth pressure at rest",
                    "units": "",
                    "isStandard": False
                },
                {
                    "codeID": "PermeabilityHorizontal",
                    "description": "Horizontal permeability",
                    "units": "m/s",
                    "isStandard": False
                },
                {
                    "codeID": "PermeabilityVertical",
                    "description": "Vertical permeability",
                    "units": "m/s",
                    "isStandard": False
                },
                {
                    "codeID": "YoungsModulusUndrained",
                    "description": "Undrained Young's modulus",
                    "units": "MPa",
                    "isStandard": True
                },
                {
                    "codeID": "PoissonsRatio",
                    "description": "Poissons ratio",
                    "units": "",
                    "isStandard": False
                },
                {
                    "codeID": "AngleDilation",
                    "description": "Angle dilation",
                    "units": "deg",
                    "isStandard": False
                },
                {
                    "codeID": "AnalysisType",
                    "description": "Material analysis type",
                    "units": "",
                    "isStandard": False
                },
                {
                    "codeID": "AnalysisDrainageCondition",
                    "description": "Drainage condition",
                    "units": "",
                    "isStandard": False
                }
            ]
        }
    ]
    return new_model

# Adds project metadata from model
def add_project_data(model, data):
    new_model = model.copy()
    
    new_model["agsProject"].update({
        "projectUUID": data["metadata"]["jobNumber"],
        "projectName": data["metadata"]["title"],
        "producer": "Arup",
        "client": "Arup",
        "description": data["metadata"]["description"],
    })
    
    return new_model

def rgba_to_hex(rgba):
    r = rgba['r']
    g = rgba['g']
    b = rgba['b']
    a = rgba['a'] 
    return '#{:02x}{:02x}{:02x}{:02x}'.format(r, g, b, a)
    
# Adds material data to model elements
def get_model_materials(data, conversion_table):
    materials = []
    
    for i, material_data in enumerate(data["stages"][0]["stageSoilBlocks"]):
        analysis_material = data["materials"][i]["analysisMaterial"]
        
        vertices_ids = data["soilBlocks"][i]["geometries"][0]["segments"][0][0]["vertices"]
        vertices = []
        for vertex_id in vertices_ids:
            for vertex in data["vertices"]:
                if vertex["id"] == vertex_id:
                    vertices.append({"x": vertex["x"], "y": vertex["y"]})
                    break
        top_elevation = max(point['y'] for point in vertices)
        bottom_elevation = min(point['y'] for point in vertices)

        values_to_ignore = [
            "YoungsModulusUndrained"
        ]
        
        agsi_data_params = []
        for index, row in conversion_table.iterrows():
            if pd.isnull(row["AGSi code"]) or pd.isnull(row["Gofer parameter"]):
                continue
            if row["AGSi code"] in values_to_ignore:
                continue
            
            if row["AGSi code"] == "Cohesion" and analysis_material.get("cohesionGradient") is not None:
                top_cohesion = analysis_material.get("cohesion") 
                distance = top_elevation - bottom_elevation
                bottom_cohesion = top_cohesion + (analysis_material.get("cohesionGradient") * distance)
                
                top_cohesion = top_cohesion * row.get("Conversion Factor", 1)
                bottom_cohesion = bottom_cohesion * row.get("Conversion Factor", 1)
                
                agsi_data_params.append({
                    "codeID": "Cohesion",
                    "caseID": "GoferOutput",
                    "valueProfileIndVarCodeID":"Elevation",
                    "valueProfile": [
                        [
                            [top_elevation, top_cohesion],
                            [bottom_elevation, bottom_cohesion]
                        ]
                    ]
                })
                continue
            
            if row["AGSi code"] == "YoungsModulusDrained" and analysis_material.get("youngsModulusGradient") is not None:
                
                top_youngs_modulus_drained = analysis_material.get("youngsModulusE1") 
                distance = top_elevation - bottom_elevation
                bottom_youngs_modulus_drained = top_youngs_modulus_drained + (analysis_material.get("youngsModulusGradient") * distance)
                
                top_youngs_modulus_drained = top_youngs_modulus_drained * row.get("Conversion Factor", 1)
                bottom_youngs_modulus_drained = bottom_youngs_modulus_drained * row.get("Conversion Factor", 1)
                        
                agsi_data_params.append({
                    "codeID": "YoungsModulusDrained",
                    "caseID": "GoferOutput",
                    "valueProfileIndVarCodeID":"Elevation",
                    "valueProfile": [
                        [
                            [top_elevation, top_youngs_modulus_drained],
                            [bottom_elevation, bottom_youngs_modulus_drained],
                        ]
                    ]
                })           
                continue
            
            value = analysis_material.get(row["Gofer parameter"])
            if value == None:
                continue
            
            coeffecient = row.get("Conversion Factor") or 1
            value = value * coeffecient 
            
            agsi_data_params.append({
                "codeID": row["AGSi code"],
                "caseID": "GoferOutput",
                "valueNumeric": value 
            })
        
        
        material = {
            "elementID": f"Material {i+1}",
            "elementName": data["materials"][i]["name"],
            "description": data["materials"][i]["name"],
            "elementType": "Geotechnical unit", 
            "geometryObject": "agsiGeometryLayer", 
			"colourRGB": rgba_to_hex(data["materials"][i]["colour"]), 
            "agsiDataParameterValue": agsi_data_params,
            "agsiGeometry": {
                "topElevation": top_elevation,
                "bottomElevation": bottom_elevation
            }
        }
        
        materials.append(material)
    
    # Couldn't find, assume consistent material
    # Potentially related to water profile?
    ground_water_level = {
        "elementID": "GWL",
        "elementName": "Design groundwater level",
        "description": "Design groundwater level",
        "elementType": "Groundwater",
        "geometryObject": "agsiGeometryPlane",
		"colourRGB":"",
        "agsiDataParameterValue": [
            {
                "codeID": "UnitWeightWater",
                "caseID": "Pile design",
                "valueNumeric": 10.0
            },
            {
                "codeID": "WaterPressure",
                "caseID": "Pile design",
                "valueNumeric": 0
            }
        ],
        "agsiGeometry": {
            "description": "design groundwater level",
            "elevation": 5
        }
    }
    materials.append(ground_water_level)
    
    return materials

# Adds the agsi model information
def add_model_data(model, data, conversion_table):
    new_model = model.copy()
    
    new_model["agsiModel"] = [
        {
            "modelID": data["id"],
            "modelName": data["metadata"]["title"],
            "description": data["metadata"]["description"],
            "coordSystemID": model["agsProject"]["agsProjectCoordinateSystem"][0]["systemID"],
            "modelType": "Geotechnical design model", 
            "category": "Analytical",
            "domain": "Geotechnical",
            "input": "",
            "method": "",
            "usage": "Design ground model for use in Oasys Gofer software",
            "uncertainty": "",
            "remarks": "",
            "agsiModelElement": get_model_materials(data, conversion_table),
            "agsiModelAlignment": [] 
        }
    ]
    
    return new_model
  
# Removes values from the template that were not assigned
def remove_ungiven_values(model):
    if isinstance(model, dict):
        new_model = model.copy()
        if 'type' in new_model and new_model['type'] in ['string', 'number', 'boolean']:
            del new_model['type']
        for k, v in new_model.items():
            new_model[k] = remove_ungiven_values(v)
        return new_model
    elif isinstance(model, list):
        new_list = model.copy()
        for i in range(len(new_list)):
            new_list[i] = remove_ungiven_values(new_list[i])
        return new_list
    else:
        return model
     
# Writes model to json file       
def export_model(model):
    with open(f'{model["agsProject"]["projectName"]}.json', 'w') as f:
        json.dump(model, f, indent=4)

def main(data):
    conversion_table = pd.read_excel(conversion_table_filename, sheet_name="Comparison", header=0)
    # Replace with GTR lookup
    model = get_template_model()
    
    model = add_static_data(model)
    model = add_project_data(model, data)
    model = add_model_data(model, data, conversion_table)
    
    # Validate with GTR
    export_model(model)

# Verify file is attached
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <filename>")
    else:
        with open(sys.argv[1], 'r') as f:
            data = json.load(f)
            main(data)
