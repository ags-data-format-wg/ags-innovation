# Oasys Gofer Model Converter

This Python script converts Oasys Gofer json models into AGSi format json files.
This is draft code for proof of concept, not a finished product

gofer.txt included as an example gofer model and will work for demo purposes

# python venv setup on windows

1. **Create a virtual environment**
    ```
    python3 -m venv venv
    ```

2. **Activate the virtual environment**
    ```
    venv\Scripts\activate
    ```

3. **Install the requirements**
    ```
    pip install -r requirements.txt
    ```

# Running the Script
converter.py <filename.txt>

Replace `<filename.txt>` with the name of your gofer model filename

Uploaded as opensource by Arup ground engineering, credit to Damion Brecknell for producing the python script.


