from dataclasses import dataclass
from enum import Enum
from typing import Any, Dict, List, Literal, Optional, TypedDict


import pandas as pd  # type: ignore


class RuleType(Enum):
    abbr = "abbr"
    eq = "eq"
    required = "required"
    gt = "gt"
    lt = "lt"


class RuleDict(TypedDict):
    type: Literal["abbr", "eq", "required", "gt", "lt"]
    group: str
    header: str
    value: Optional[Any]


@dataclass
class AgsError:
    line_number: int
    rule: RuleDict
    value: Any
    message: Optional[str] = None


@dataclass
class AgsTable:
    name: str
    start_line: int
    columns: List[str]
    units: Dict[str, str]
    rows: pd.DataFrame

    def __post_init__(self):
        data_start = self.start_line + 4
        self.rows.index = range(data_start, data_start + len(self.rows))
