import os
from pathlib import Path

cur = os.path.dirname(os.path.abspath(__file__))
ABBR_PATH = Path(cur) / "ABBR.csv"
RULES_PATH = Path(cur) / "validation.csv"
