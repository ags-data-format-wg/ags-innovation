import json
from typing import Dict, List

import pandas as pd  # type: ignore

from ags_quality_checker.io import parse, AGSTableDict
from ags_quality_checker.config import RULES_PATH, ABBR_PATH
from ags_quality_checker.models import (
    AgsError,
    AgsTable,
    RuleDict,
)

# current file path
_ABBR_DATA = pd.read_csv(ABBR_PATH)
_RULES_DATA = pd.read_csv(RULES_PATH).to_dict(orient="records")


class AgsChecker:

    def __init__(self, tables: Dict[str, AGSTableDict]) -> None:

        self.tables = {
            table_name: AgsTable(
                name=table_name,
                start_line=table_data["start_line"],
                columns=table_data["columns"],
                units=table_data["units"],
                rows=pd.DataFrame(table_data["rows"], columns=table_data["columns"]),
            )
            for table_name, table_data in tables.items()
        }

    def validate(self) -> List[AgsError]:
        errors = []
        for rule in _RULES_DATA:
            if rule["type"] == "abbr":
                errors.extend(self._validate_abbr(rule))
            elif rule["type"] == "eq":
                errors.extend(self._validate_eq(rule))
            elif rule["type"] == "required":
                errors.extend(self._validate_required(rule))
            elif rule["type"] == "gt":
                errors.extend(self._validate_gt(rule))
            elif rule["type"] == "lt":
                errors.extend(self._validate_lt(rule))
        return errors

    def _validate_required(self, rule: RuleDict) -> List[AgsError]:

        table = self.tables[rule["group"]].rows

        missing_rows = table.loc[
            table[rule["header"]].isna() | (table[rule["header"]] == ""), rule["header"]
        ]

        errors = []
        for i, value in missing_rows.items():
            errors.append(
                AgsError(
                    line_number=i,
                    rule=rule,
                    value=value,
                    message=f"{rule['header']} is required but missing.",
                )
            )

        return errors

    def _validate_abbr(self, rule: RuleDict) -> List[AgsError]:
        table = self.tables[rule["group"]].rows
        incorrect = table.loc[
            table[rule["header"]].isin(
                _ABBR_DATA.loc[_ABBR_DATA["Heading"] == rule["header"], "ABBR_CODE"]
            )
        ]
        return [
            AgsError(
                line_number=index,
                rule=rule,
                value=value,
                message=f"Invalid ABBR code for {rule['header']}. Provided {value}",
            )
            for index, value in incorrect[rule["header"]].items()
        ]

    def _validate_eq(self, rule: RuleDict) -> List[AgsError]:
        return []

    def _validate_gt(self, rule: RuleDict) -> List[AgsError]:
        return []

    def _validate_lt(self, rule: RuleDict) -> List[AgsError]:
        return []

    @classmethod
    def parse_string(cls, data: str) -> "AgsChecker":
        parsed = parse(data)
        return cls(parsed)
