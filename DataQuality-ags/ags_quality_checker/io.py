from typing import Dict, Any, List, TypedDict
from pprint import pprint


class AGSTableDict(TypedDict):
    name: str
    start_line: int
    columns: List[str]
    units: Dict[str, str]
    rows: List[List[str]]
    types: Dict[str, str]


def parse(data: str) -> Dict[str, AGSTableDict]:

    lines = data.split("\n")
    # we need to keep line numbers for error reporting

    # find the start line numbers of each table
    table_bounds = {}
    for i, line in enumerate(lines):
        if '"GROUP"' in line:
            table_name = line.split(",")[1].replace('"', "")
            table_bounds[table_name] = i + 1

    tables = data.split('"GROUP",')

    # remove the first table if it is empty string
    if tables[0].strip() == "":
        tables = tables[1:]

    table_texts = {}
    for table in tables:
        # print(table)
        table_lines = table.split("\n")
        name = table_lines[0].replace('"', "")

        # remove HEADING and double quotes
        columns = table_lines[1].split('","')
        columns = [column.replace('"', "") for column in columns]
        columns = columns[1:]

        # remove UNIT and double quotes
        units = table_lines[2].split('","')
        units = [unit.replace('"', "") for unit in units]
        units = units[1:]
        units_dict = dict(zip(columns, units))

        types = table_lines[3].split('","')
        types = [type.replace('"', "") for type in types]
        types = types[1:]
        types_dict = dict(zip(columns, types))

        data_rows = table_lines[4:]

        rows = [row.split('","')[1:] for row in data_rows]
        rows = [row for row in rows if row]
        rows = [[item.replace('"', "") for item in row] for row in rows]

        # print(table_lines)
        ags_table: AGSTableDict = {
            "name": name,
            "start_line": table_bounds[name],
            "columns": columns,
            "units": units_dict,
            "rows": rows,
            "types": types_dict,
        }
        table_texts[name] = ags_table

    return table_texts
